---
title: Checklist / Course preparation
teaching: 0
exercises: 0
questions:
- "Setup tasks to be completed **BEFORE** the course starts."
objectives:
- "Prepare a computer for the course."
- "Get access to all systems needed for in the course."
keypoints:
- "Do this early!"
- "Talk to your admin, then to the instructors."
---

## Checklist for the course

Please make sure to check all of this _early_.  Some of the technical steps
might include waiting for an administrator to install something.

- Have a laptop for the course?
- Have WiFi access in the west-shore building?
- Have an account at git.geomar.de?
- Have Git on your laptop?
- Have an SSH key to git.geomar.de?

If any of these fail, talk to your group's or institute's admin.  If they can't
help, see if the instructor(s) of this course can help.

## Have a laptop you can use for the course?

You will need a laptop which is connected to the internet.  Talk to the
instructor(s) of this course if you have no access to a portable machine.  The
might be able to help.


## Have a WiFi connection (Eduroam) in the west-shore building?

For Session 2, you need access to the internet (in particular git.geomar.de).
Please make sure that from your laptop, you can access: <https://git.geomar.de>

Talk to the IT-Department or your local computer person if this is not the
case.


## Have an account at git.geomar.de?

1. Go to git.geomar.de and login using your full Geomar email alias and your
   portal password.  If you succeed.  You're done and can skip step 2.

2. If this fails, you need to reset / initialize your Password.  Go to
   <https://portal.geomar.de/web/data-management/home> and follow instructions
   there.  (I've checked with the data management to make sure all of you have
   an account with the Geomar-Data-Portal.)


## Have Git on your computer?

On Linux, try `git --version` on the command line and, if necessary, install
Git using your package manager or talk to the person managing your computer.

On Mac, try `git --version` in a terminal and, if necessary, install Git by
following: <https://www.atlassian.com/git/tutorials/install-git#mac-os-x> Note
that (at least in Sierra), `git` may be pointing to the pre-installed git in
`/user/bin`.  Make sure to use `/usr/local/git/bin/git` instead.  (Again, talk
to your admin if you're not sure how to do this.)

On Windows, follow:
<https://www.atlassian.com/git/tutorials/install-git#windows>


## Have an SSH key to git.geomar.de?

When everything else works, please follow the instructions on
<https://git.geomar.de/help/ssh/README.md#generating-a-new-ssh-key-pair> to add
a key allowing for password-less work.
