---
title: Conflicts
teaching: 20
exercises: 5
questions:
- "What do I do when my changes conflict with someone else's?"
objectives:
- "Explain what conflicts are and when they can occur."
- "Resolve conflicts resulting from a merge."
keypoints:
- "Conflicts occur when two or more people change the same file(s) at the same time."
- "The version control system does not allow people to overwrite each other's changes blindly, but highlights conflicts so that they can be resolved."
---

As soon as people can work in parallel, it's likely someone's going to step on
someone else's toes.  This will even happen with a single person: if we are
working on a piece of software on both our laptop and a server in the lab, we
could make different changes to each copy.  Version control helps us manage
these [conflicts]({{ page.root }}/reference/#conflicts) by giving us tools to
[resolve]({{ page.root }}/reference/#resolve) overlapping changes.


## Story

Imagine the following scenario:

- Two people work on the `planets` project and create a file called
  `neptune.txt` which only contains a headline reading and the information that
  Neptune is planet number eight.
- But one of them likes to spell out `eighth planet` while the other likes the
  shorter notation `8th planets` more.
- Both try to merge into their changes.


## Person 1 starts working

Person 1 remembers that it is safer to branch from master and only merge after
everything is in order.  So they first check where they are (with `git
branch`), and then create a branch `add-8th-planet`:

~~~
$ git branch
~~~
{: .bash}
~~~
* master
~~~
{: .output}
~~~
$ git checkout -b add-8th-planet
~~~
{: .bash}
~~~
Switched to a new branch 'add-8th-planet'
~~~
{: .output}

Then, the file `neptune.txt` is filled with some minimal content:

~~~
$ nano neptune.txt
~~~
{: .bash}
~~~
# Neptune
Neptune is the 8th planet.
~~~
{: .output}

And the change is added to the staging area (with `git add`) and then
committed.

~~~
$ git add neptune.txt
$ git commit -m "Add stub for Neptune."
~~~
{: .bash}
~~~
[add-8th-planet a650041] Add stub for Neptune.
 1 file changed, 2 insertions(+)
 create mode 100644 neptune.txt
~~~
{: .output}


## Person two has the same idea

(Note that we leave the question "How do I collaborate anyway?" for later...)

Person two is careless and starts editing the master brach right away:

~~~
$ git checkout master
$ nano neptune.txt
~~~
{: .bash}
~~~
# Neptune
Neptune is the eighth planet.
~~~
{: .output}

And the change is added to the staging area (with `git add`) and then
committed.

~~~
$ git add neptune.txt
$ git commit -m "Add first draft for Neptune."
~~~
{: .bash}
~~~
[master 1ebcc82] Add first draft for Neptune.
 1 file changed, 2 insertions(+)
  create mode 100644 neptune.txt
~~~
{: .output}


## Person 1 is done and wants to merge to master

Person 1 checks out `master` and tries to merge it:

~~~
$ git checkout master
$ git merge add-8th-planet
~~~
{: .bash}
~~~
Auto-merging neptune.txt
CONFLICT (add/add): Merge conflict in neptune.txt
Automatic merge failed; fix conflicts and then commit the result.
~~~
{: .output}

Git finds a conflict between the changes in `master` and those in
`add-8th-planet`.  It stops to let the user solve the conflicts and asks them
to commit the changes afterwards.

Let's first inspect where we are.

~~~
$ git status
~~~
{: .bash}
~~~
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")

Unmerged paths:
  (use "git add <file>..." to mark resolution)

	both added:      neptune.txt

no changes added to commit (use "git add" and/or "git commit -a")
~~~
{: .output}


## Conflict markers

So `git status` knows about the merge in process and tells us what to do:  `Fix
conflicts and run git commit.`

> ## _
>
> Please, do really look at the output of Git.  Most of the time, you are
> guided through any difficult situation.
>
{: .callout}

The file `neptune.txt` now contains markers
indicating the difference between the two versions.

~~~
$ cat neptune.txt
~~~
{: .bash}
~~~
# Neptune
<<<<<<< HEAD
Neptune is the eighth planet.
=======
Neptune is the 8th planet.
>>>>>>> add-8th-planet
~~~
{: .output}

The line between `<<<<<<< HEAD` and `=======` contains the version of line 2
from `HEAD` (which is pointing to the master branch).  The line between
`=======` and `>>>>>>> add-8th-planet` shows line 2 from the branch
`add-8th-planet`.

Person 1 resorts to middle ground and re-formulates the line in question:

~~~
$ nano neptune.txt
~~~
{: .bash}
~~~
# Neptune
Neptune is planet 8.
~~~
{: .output}

Then, the resolution is added and committed:

~~~
$ git add neptune.txt
$ git commit
~~~
{: .bash}

On the way, the standard editor pops up and shows

~~~
Merge branch 'add-8th-planet'

# Conflicts:
#	neptune.txt
#
# It looks like you may be committing a merge.
# If this is not correct, please remove the file
#	.git/MERGE_HEAD
# and try again.


# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch master
# All conflicts fixed but you are still merging.
#
# Changes to be committed:
#	modified:   neptune.txt
#
~~~
{: .output}

After closing (or editing, saving and closing) this message, the following is
written to the shell:

~~~
[master 3628b44] Merge branch 'add-8th-planet'
~~~
{: .output}


## History

We can inspect this history:

~~~
$ git log --oneline --graph --decorate
~~~
{: .bash}
~~~
*   3628b44 (HEAD -> master) Merge branch 'add-8th-planet'
|\
| * a650041 (add-8th-planet) Add stub for Neptune.
* | 1ebcc82 Add first draft for Neptune.
|/
* 75185ab (...)
~~~
{: .output}

Do you see the flow of information here?

> ## ASCII Art
>
> Git log is very powerful ...
>
> ~~~
> $ git log \
> $     --graph \
> $     --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' \
> $     --abbrev-commit
> ~~~
> {: .bash}
>
> ![A very advanced view](../fig/pretty_annotated_and_decorated_git_log.png)
>
> *Excursion:* Discuss good and bad commit messages.
>
{: .challenge}

------
