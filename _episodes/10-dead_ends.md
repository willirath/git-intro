---
title: Dead ends
teaching: 10
exercises: 0
questions:
- "How can I handle ideas that won't make it into master?"
objectives:
- "Learn how to handle dead ends, and how to keep or discard 'bad ideas'."
keypoints:
- "If there are un-tracked files you want to get rid of: `git clean -i`"
- "Remove (merged) branches with: `git branch -d <branch name>`"
- "Capital `-D` forces removal of unmerged branch: `git branch -D <branch name>`"
---


## Basic idea

Imagine (or remind yourself of the feeling of) enthusiastically start working
on a new idea which proves infeasible after a few steps.

How do you return to master in a clean way?


## An example: Trojans

To be more specific, let's expolore a stupid idea in the planets project:
Trojans of Jupiter.  [There can't be that many...][solar-system-with-trojans]

~~~
$ git branch trojans
$ git checkout trojans
$ touch jupiters_trojans.txt
$ git add jupiters_trojans.txt
$ git commit -m "Create file to describe all trojans of Jupiter."
~~~
{: .bash}

Then, you start adding one section for each of them ([see the full list of full
lists here][list_trojans]):

~~~
# 617 Patroclus

617 Patroclus is a binary minor planet made up of two objects of similar size
orbiting their barycenter.

# 884 Priamus

884 Priamus, provisional designation 1917 CQ, is a rare-type Jupiter trojan
from the Trojan camp, approximately 100 kilometers in diameter.

# 1172 AEneas

...
~~~
{: .output}

You quickly learn that there are two camps of Jupiter trojans (the Trojan camp
and the Greek camp).  So you start editing a second file
`jupiters_trojans_greek.txt`.  But before long, you realize that there's almost
a million of them.

So let's discard this idea.

~~~
$ git checkout master
~~~
{: .bash}

Or is it that simple?

~~~
$ git status
~~~
{: .bash}

tells us that there's an untracked file `jupiters_trojans_greek.txt`
and

~~~
$ git branch
~~~
{: .bash}

still knows about `trojans`.


## Cleaning up the working tree

You can clean up the working tree by deleting untracked files:

~~~
$ git clean -i
~~~
{: .bash}

This will tell you what will be done and ask for a confirmation before actually
removing anything.


## Removing branches

The dead-end branch `trojans` can be removed with

~~~
$ git branch -D trojans
~~~
{: .bash}


## But consider to keep the dead ends

There are different opinions about whether it is a good idea to remove bad
ideas from history (by deleting them) or if dead ends should live forever or at
least be archived in some way.


[solar-system-with-trojans]: https://upload.wikimedia.org/wikipedia/commons/f/f3/InnerSolarSystem-en.png
[list_trojans]: https://en.wikipedia.org/wiki/List_of_Jupiter_trojans_(Trojan_camp)

