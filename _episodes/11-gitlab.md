---
title: Remotes in Gitlab
teaching: 30
exercises: 0
questions:
- "How do I share my changes with others?"
- "How do I use Gitlab?"
objectives:
- "Explain what remote repositories are and why they are useful."
- "Push to or pull from a remote repository."
keypoints:
- "Even before this episode, you had a very powerful version control system."
- "A local Git repository can be connected to one or more remote repositories."
- "Use the HTTPS protocol to connect to remote repositories if you did not set up SSH yet."
- "`git push` copies changes from a local repository to a remote repository."
- "`git pull` copies changes from a remote repository to a local repository."
---

> ## Local-only Git already is very powerful
>
> Before (for the first time in this course) learning how to use Git with
> remote repositories, let's take a second to recapitulate that we already have
> a very powerful tool for local-only use.  You already know all you need to
> keep a digital journal while at sea without access to anything but your
> laptop for weeks or months.
>
> > ## Realistic use cases for local-only Git
> >
> > - Have this very old serial terminal to read oxygen sensors in the lab
> >   which sometimes suddenly remembers the name of the previous output file
> >   and decides to just write there?  ---  Track each step in a commit.
> >
> > - Explore ideas for a blog post about your latest Argo deployment in
> >   collaboration with your own sleep-deprived self from just after the last
> >   night shift?  ---  Branch if you're not sure you can trust your
> >   reasoning.
> >
> > - ...
> >
> {: .solution}
{: .callout}

## Where to host repositories?

Version control really comes into its own when we begin to collaborate with
other people.  We already have most of the machinery we need to do this; the
only thing missing is to copy changes from one repository to another.

Systems like Git allow us to move work between any two repositories.  In
practice, though, it's easiest to use one copy as a central hub, and to keep it
on the web rather than on someone's laptop.  Most programmers use hosting
services like [GitHub](http://github.com), [BitBucket](http://bitbucket.org) or
[GitLab](http://gitlab.com/) to hold those master copies.

## Create a project / repository

Let's start by sharing the changes we've made to our current project with the
"world".  Log in to Geomar's Gitlab server <https://git.geomar.de>, then click
on the icon in the top right corner to create a new project called `planets`:

![Creating a Project on Gitlab (Step 1)](../fig/gitlab_01_initial_view.png)

Name your repository "planets" and then click "Create Project":

![Creating a Project on Gitlab (Step 2)](../fig/gitlab_02_create_project.png)

As soon as the project is created, Gitlab displays a page with a URL and some
information on how to configure your local repository:

![Creating a Project on Gitlab (Step 3)](../fig/gitlab_03_created_project.png)

## So what's happening on the server?

This effectively does the following on the Gitlab server:

~~~
$ mkdir planets
$ cd planets
$ git init
~~~
{: .bash}

Our local repository still contains our earlier work we did on the planets, but
the remote repository on Gitlab doesn't contain any files yet:

![Freshly-Made Gitlab Repository](../fig/git-freshly-made-gitlab-repo.svg)

## Connect the remote repository with our existing local one

The next step is to connect the two repositories.  We do this by making the
Gitlab repository a [remote]({{ page.root }}/reference/#remote) for the local
repository.  The home page of the repository on Gitlab (see above) includes the
string we need to identify it:  They provide a url to the remote repository.

> ## HTTPS vs. SSH
>
> HTTPS does not require additional configuration.  It is, however, preferrable
> to set up SSH access which makes it easier to work without having to re-type
> your login data on every commit / push.  To setup SSH access, read one of the
> tutorials from
> [GitHub](https://help.github.com/articles/generating-ssh-keys),
> [Atlassian/BitBucket](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git)
> and [GitLab](https://about.gitlab.com/2014/03/04/add-ssh-key-screencast/)
> (this one has a screencast).
{: .callout}

Copy the URL that is displayed, go into the local `planets` repository, and run
this command:

~~~
$ git remote add origin git@git.geomar.de:<your-username>/planets.git
~~~
{: .bash}

Make sure to use the URL for your repository rather than the dummy-one provided
here.

## Check which remotes are known

We can check that the command has worked by running `git remote -v` in the
local repository:

~~~
$ git remote -v
~~~
{: .bash}
~~~
origin   git@git.geomar.de:<your-username>/planets.git (push)
origin   git@git.geomar.de:<your-username>/planets.git (fetch)
~~~
{: .output}

The name `origin` is a local nickname for your remote repository: we could use
something else if we wanted to, but `origin` is by far the most common choice.

## Push changes to the remote

Once the nickname `origin` is set up, this command will push the changes from
our local repository to the repository on GitHub:

~~~
$ git push -u origin master
~~~
{: .bash}
~~~
Counting objects: 9, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (9/9), 821 bytes, done.
Total 9 (delta 2), reused 0 (delta 0)
To git@git.geomar.de:<your-username>/planets.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
~~~
{: .output}

Our local and remote repositories are now in this state:

![Gitlab Repository After First Push](../fig/gitlab-repo-after-first-push.svg)

> ## The '-u' Flag
>
> We used `git push -u origin master`.  The `-u`-option associates the local
> branch `master` with `origin/master` which is our nickname for `master` in
> the remote repository.  This way, we can later just use `git push` and Git
> knows where to push.
>
{: .callout}

## Pulling changes from the remote

We can pull changes from the remote repository to the local one as well:

~~~
$ git pull
~~~
{: .bash}
~~~
From git@git.geomar.de:<your-username>/planets.git
 * branch            master     -> FETCH_HEAD
Already up-to-date.
~~~
{: .output}

Pulling has no effect in this case because the two repositories are already
synchronized.  If someone else had pushed some changes to the repository on
GitLab, though, this command would download them to our local repository.

---

> ## Gitlab GUI
>
> Browse to your `planets` project on Gitlab.  "Repository" shows a listing of
> all files / directory contained in the project.  Take some time to click
> around and see if you find a history of changes.
>
{: .challenge}

> ## Push vs. Commit
>
> In this lesson, we introduced the "git push" command.  How is "git push"
> different from "git commit"?
>
{: .challenge}

> ## Fixing Remote Settings
>
> It happens quite often in practice that you made a typo in the
> remote URL. This exercice is about how to fix this kind of issues.
> First start by adding a remote with an invalid URL:
>
> ~~~
> git remote add broken git@git.geomar.de:<your-username>/planetts.git
> ~~~
> {: .bash}
>
> Do you get an error when adding the remote? Can you think of a command that
> would make it obvious that your remote URL was not valid? Can you figure out
> how to fix the URL (tip: use `git remote -h`)? Don't forget to clean up and
> remove this remote once you are done with this exercise.
>
> > ## Solution
> >
> > - `git fetch broken` will try to collect information from the remote.  It
> >   will fail if the remote repository cannot be found.
> >
> > - `git remote remove broken` will remove the broken remote.
> >
> {: .solution}
{: .challenge}

> ## Gitlab License and README files
>
> In this section we learned about creating a remote repository on Gitlab, but
> when you initialized your Gitlab repo, you didn't add a README.md or a
> license file. If you had, what do you think would have happened when you
> tried to link your local and remote repositories?
>
> > ## Solution
> >
> > Adding files on the server will result in an `origin/master` differing from
> > its local counter part.  Hence, pushing to to `origin/master` will result
> > in an error telling you to first merge the two differing branches.  Pulling
> > will try an auto-merge from `origin/master` to `master`.
> >
> {: .solution}
{: .challenge}

---
