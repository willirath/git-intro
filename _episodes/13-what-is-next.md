---
title: What's next?
teaching: 10
exercises: 0
questions:
- "How do I go on from here?"
objectives:
- "Collect resources."
keypoints:
---

## Builtin help

Running commands like

~~~
$ git help
$ git help branch
$ git help commit
$ git help <command>
~~~
{: .bash}

will print very detailed and very helpful introductions to the various Git
commands.


## Official Git Website

- <https://git-scm.com> offers downloads, manuals and documentation.  This is a
very good starting point for those who know what they are looking for.

- <https://git-scm.com/docs> is the official reference manual.  This basically
  matches what you also get with `git help`.  But you might like the website
  approach more than reading man pages in a terminal.

- <https://git-scm.com/book/en> is the Pro Git book.  It explains concepts in
  often easily digestible chapters.  (You might recognise the Chapter on
  branches which was the foundation for the episode on branches in this
  course.)  (There also is a German version: <https://git-scm.com/book/de>)


## Short references

- A very good reference card:
  <https://www.atlassian.com/dms/wac/images/landing/git/atlassian_git_cheatsheet.pdf>


## Tutorials

(Note that all these tutorials are focussed on one of the three main
server-based Git solutions Github, Gitlab, and Bitbucket.)

- <https://www.atlassian.com/git/tutorials> nicely presented tutorial based on
  Bitbucket.

- <https://docs.gitlab.com/ce/gitlab-basics/README.html> intro focussed on
  Gitlab.

- <https://try.github.io> a nice and short intro to basic commands.

--------
