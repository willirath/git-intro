---
title: Branches
teaching: 25
exercises: 0
questions:
- "How can I work on a feature without spoiling my main code?"
objectives:
- "Create a branch, commit there, and merge it back."
- "Understand the flow of information in a simple workflow using branch, commit, merge."
keypoints:
- "`git brach <branch name>` creates a branch."
- "`git checkout <branch name>` switches to this new branch."
- "`git checkout master` switches back to the master branch."
- "`git merge <branch name>` merges branch `<branch name>` into the current branch."
- "`git branch -d <branch name>` deletes the branch."
---


Note that the ideas described in this episode will be the main foundation for
collaborative work described later.  Note also that this episode heavily leans
on [Chapter 3][progit2-chapter-03] of the [Pro Git book][progit2-book].

[progit2-chapter-03]: https://www.git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell
[progit2-book]: https://www.git-scm.com/book/en/v2


## A typical work flow

We often (most of the time?) do not work in a linear way where we advance by
consecutive steps, never go back, or are stuck in dead ends.  Our work flows
often include concurrent changes to different parts of our work.  We, e.g.,
work on an introductory section of a chapter while in a different window adding
things to anoter part of the same document.

The basic building block of this workflow looks like this:

![Branch your project, modify, and merge.](../fig/branch_01.svg)


## Outline of what is following

We will deviate from our planets project for a while and imagine the following
situation:

- A project with only a `master` branch has an issue (call it `iss53`) wich
  needs fixing.

- To leave the code used in production unchanged until we are sure that our fix
  to `iss53` works smoothly, we create a branch called `iss53`, develop our
  solution there, and only merge our changes back into the main branch when we
  are done.

- Before `iss53` is ready to be used in production, a severe problem in
  `master` needs to be addressed immediately.  We hence create a branch
  `hotfix`, solve the more urgent problem, and only then return to `iss53`.

- In the end, we will merge our solution for `iss53` back to `master` which by
  that time has advanced from where we started when creating `iss53`.


## Initial state

We start with a linear history and a branch `master` pointing to commit `C2`.

![Master at C2](../fig/progit2_basic-branching-1.png)


## Start working on an issue

Before changing any code, we create a branch called `iss53` which initially
points to the same commit as `master`:

~~~
$ git branch iss53
$ git checkout iss53
~~~
{: .bash}

![Master and iss53 at C2](../fig/progit2_basic-branching-2.png)

We then work on our issue 53, change something, commit it, and hence advance
the branch `iss53` to commit `C3`.

~~~
$ nano some_file.py
$ git add some_file.py
$ git commit -m "Fix something to partly address issue 53."
~~~
{: .bash}

![Master at C2 and iss53 at C3](../fig/progit2_basic-branching-3.png)


## Switch back to morge urgent problem

Now, we go back to `master`, create a branch `hotfix`, edit something, commit
it:

~~~
$ git checkout -b hotfix
$ nano some_other_file.py
$ git add some_other_file.py
$ git commit -m "Adress some urgent problem, cross fingers..."
~~~
{: .bash}

This advances the newly created branch `hotfix` to commit `C4`:

![Master at C2, iss53 at C3, hotfix at C4](../fig/progit2_basic-branching-4.png)


## Merge `hotfix` to `master`

After some testing, we are confident that `hotfix` is ready to be used in
production.  We merge it into `master` by first checking out `master` and then
merging **from** `hotfix`:

~~~
$ git checkout master
$ git merge hotfix
~~~
{: .bash}

As `master` was just one commit behind `hotfix`, we can *fast forward* `master`
to `C4`.  This leaves us with:

![Master and hotfix at C4, iss53 at C3](../fig/progit2_basic-branching-5.png)


## Delete `hotfix`

The branch `hotfix` is not needed anymore.  We delete it:

~~~
$ git branch -d hotfix
~~~
{: .bash}

Note that this **does not delete any commits** we just remove a pointer called
`hotfix` which is currently pointing to commit `C4`:

![Master at C4, iss53 at C3](../fig/progit2_basic-branching-5a.png)


## Continue working on issue 53

After the urgent problem has been taken care of, we can return to issue 53 by
checking out the branch `iss53`.  There, we edit something, and track the
chages in commit `C5`:

~~~
$ nano some_file.html
$ git add some_file.html
$ git commit -m "Finally fully addressed issue 53."
~~~
{: .bash}

But as we want to merge `iss53` into `master` we note that our history has
diverged:  We have one line of history `C0<--C1<--C2<--C4` which [is currently
pointed at](https://m.xkcd.com/1597/) by `master` and one line
`C0<--C1<--C2<--C3<--C5` which is currently pointed at by `iss53`.

![Master at C4, iss53 at
C5](../fig/progit2_basic-branching-6.png)


## Merge `iss53` into `master`

~~~
$ git checkout master
$ git merge iss53
~~~
{: .bash}

And (in the somewhat constructed ideal example presented here) this just works.


## But what is going on?

When merging the snapshot `C5` into the snapshot `C4`, Git first determines
their latest common ancestor to be `C2` and then does a three-way merge using
`C4`, `C2`, and `C5`.

![Master at C4, iss53 at C5, highlighted ancestor C2](../fig/progit2_basic-merging-1.png)

The history we created here looks as follows:

![Master at a newly created C6, iss53 remained at C5](../fig/progit2_basic-merging-2.png)

Note that only the targed branch (`master` in this case) is advanced to `C6`
while the source branch of the merge, `iss53`, remains at `C5`.


## Delete `iss53`

Like with `hotfix`, we now delete the branch `iss53`:

~~~
$ git branch -d iss53
~~~
{: .bash}

![Master at a newly created C6 which has parents C5 and C4](../fig/progit2_basic-merging-3.png)


> ## Orphaned commits and deleting branches
>
> `git branch -d <branch name>` will only delete branches which are merged.
> That is, it will refuse to delete a branch if this results in orphaned
> commits.
>
> Orphaned commits are commits which cannot be reached starting from any branch
> and following history backwards.
>
> If we'd try to delete `iss53` here:
>
> ![Master at C4, iss53 at C5, highlighted ancestor C2](../fig/progit2_basic-merging-1.png)
> 
> we'd leave `C5` and `C3` orphaned or un-reachable.
>
> Check the help of `git branch` to find out how to force deleting an un-merged
> branch.
>
> > ## Solution
> >
> > `git branch -D <branch name>` or `git branch -d -f <branch name>` will
> > delete an un-merged branch.
> >
> {: .solution}
{: .challenge}


## Conflicts

The next episode will cover merges with conflicts.
