---
title: Tracking Changes
teaching: 25
exercises: 10
questions:
- "How do I record changes in Git?"
- "How do I check the status of my version control repository?"
- "How do I record notes about what changes I made and why?"
objectives:
- "Go through the modify-add-commit cycle for one or more files."
- "Explain where information is stored at each stage of Git commit workflow."
keypoints:
- "`git status` shows the status of a repository."
- "Files can be stored in a project's working directory (which users see), the staging area (where the next commit is being built up) and the local repository (where commits are permanently recorded)."
- "`git add` puts files in the staging area."
- "`git commit` saves the staged content as a new commit in the local repository."
- "Always write a log message when committing changes."
- "Prefer editing commit messages in a text editor to using the `-m` flag."
---

## Create a file

Let's create a file called `mars.txt` that contains some notes about the Red
Planet's suitability as a base.  (We'll use `nano` to edit the file; you can
use whatever editor you like.  In particular, this does not have to be the
`core.editor` you set globally earlier.)

~~~
$ nano mars.txt
~~~
{: .bash}

Type the text below into the `mars.txt` file:

~~~
Cold and dry, but everything is my favorite color
~~~
{: .output}

`mars.txt` now contains a single line, which we can see by running:

~~~
$ ls
~~~
{: .bash}

~~~
mars.txt
~~~
{: .output}

~~~
$ cat mars.txt
~~~
{: .bash}

~~~
Cold and dry, but everything is my favorite color
~~~
{: .output}

## Staging changes

If we check the status of our project again, Git tells us that it's noticed the
new file:

~~~
$ git status
~~~
{: .bash}

~~~
On branch master

Initial commit

Untracked files:
   (use "git add <file>..." to include in what will be committed)

	mars.txt
nothing added to commit but untracked files present (use "git add" to track)
~~~
{: .output}

Let's, again, take the time to read the whole output.

- The first two statements tell us where we are (branch `master` and `initial
  commit`).
- The "untracked files"-message means that there's a file in the directory that
  Git isn't keeping track of.
- We can tell Git to track a file using `git add`.

So, let's do this

~~~
$ git add mars.txt
~~~
{: .bash}

and then check that the right thing happened:

~~~
$ git status
~~~
{: .bash}

~~~
On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   mars.txt

~~~
{: .output}

## Commit changes

Git now knows that it's supposed to keep track of `mars.txt`,
but it hasn't recorded these changes as a commit yet.
To get it to do that, we need to run one more command:

~~~
$ git commit
~~~
{: .bash}
~~~
Start notes on Mars as a base

- Note on temperature
- Note on water supply
- Note on energy distribution in the visible spectrum

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch master
#
# Initial commit
#
# Changes to be committed:
#	new file:   mars.txt
#
~~~
{: .output}
~~~
[master (root-commit) f22b25e] Start notes on Mars as a base
 1 file changed, 1 insertion(+)
 create mode 100644 mars.txt
~~~
{: .output}

When we run `git commit`, Git takes everything we have told it to save by using
`git add` and stores a copy permanently inside the special `.git` directory.
This permanent copy is called a [commit]({{ page.root }}/reference/#commit) (or
[revision]({{ page.root }}/reference/#revision)) and its short identifier is
`f22b25e` (Your commit may have another identifier.)

> ## Commit messages
>
> Note that we could have used
>
> ~~~
> $ git commit -m "Start notes on Mars as a base"
> ~~~
> {: .bash}
>
> and achieved vitually the same (without the bullet points, though).
>
> As especially in collaborative projects, it is a good idea to provide
> detailed commit messages, we'll use the long-form of committing and the
> editing the commit message witha text editor wherever applicable.
>
> [Good commit messages][commit-messages] start with a brief (<50 characters)
> summary of changes made in the commit.  If you want to go into more detail,
> add a blank line between the summary line and your additional notes.
{: .callout}

If we run `git status` now:

~~~
$ git status
~~~
{: .bash}

~~~
On branch master
nothing to commit, working directory clean
~~~
{: .output}

it tells us everything is up to date.

## The log

If we want to know what we've done recently, we can ask Git to show us the
project's history using `git log`:

~~~
$ git log
~~~
{: .bash}

~~~
commit f22b25e3233b4645dabd0d81e651fe074bd8e73b
Author: Vlad Dracula <vlad@tran.sylvan.ia>
Date:   Thu Aug 22 09:51:46 2013 -0400

    Start notes on Mars as a base

    - Note on temperature
    - Note on water supply
    - Note on energy distribution in the visible spectrum
~~~
{: .output}

`git log` lists all commits  made to a repository in reverse chronological
order.  The listing for each commit includes the commit's full identifier
(which starts with the same characters as the short identifier printed by the
`git commit` command earlier), the commit's author, when it was created, and
the log message Git was given when the commit was created.

> ## Where Are My Changes?
>
> If we run `ls` at this point, we will still see just one file called `mars.txt`.
> That's because Git saves information about files' history
> in the special `.git` directory mentioned earlier
> so that our filesystem doesn't become cluttered
> (and so that we can't accidentally edit or delete an old version).
{: .callout}

Now suppose Dracula adds more information to the file.  (Again, we'll edit with
`nano` and then `cat` the file to show its contents; you may use a different
editor, and don't need to `cat`.)

~~~
$ nano mars.txt
$ cat mars.txt
~~~
{: .bash}

~~~
Cold and dry, but everything is my favorite color
The two moons may be a problem for Wolfman
~~~
{: .output}

When we run `git status` now, it tells us that a file it already knows about
has been modified:

~~~
$ git status
~~~
{: .bash}

~~~
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   mars.txt

no changes added to commit (use "git add" and/or "git commit -a")
~~~
{: .output}

The last line is the key phrase: "no changes added to commit".  We have changed
this file, but we haven't told Git we will want to save those changes (which we
do with `git add`) nor have we saved them (which we do with `git commit`).

## Diff

So let's do that now. It is good practice to always review our changes before
saving them. We do this using `git diff`.  This shows us the differences
between the current state of the file and the most recently saved version:

~~~
$ git diff
~~~
{: .bash}

~~~
diff --git a/mars.txt b/mars.txt
index df0654a..315bf3a 100644
--- a/mars.txt
+++ b/mars.txt
@@ -1 +1,2 @@
 Cold and dry, but everything is my favorite color
+The two moons may be a problem for Wolfman
~~~
{: .output}

The output is cryptic because it is actually a series of commands for tools
like editors and `patch` telling them how to reconstruct one file given the
other. If we break it down into pieces:

1.  The first line tells us that Git is producing output similar to the Unix
    `diff` command comparing the old and new versions of the file.
2.  The second line tells exactly which versions of the file Git is comparing;
    `df0654a` and `315bf3a` are unique computer-generated labels for those
    versions.
3.  The third and fourth lines once again show the name of the file being
    changed.
4.  The remaining lines are the most interesting, they show us the actual
    differences and the lines on which they occur.  In particular, the `+`
    marker in the first column shows where we added a line.

## Workflow: Add, then commit

After reviewing our change, it's time to commit it:

~~~
$ git commit
$ git status
~~~
{: .bash}

~~~
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   mars.txt

no changes added to commit (use "git add" and/or "git commit -a")
~~~
{: .output}

Whoops: Git won't commit because we didn't use `git add` first.

Let's fix that:

~~~
$ git add mars.txt
$ git commit
~~~
{: .bash}
~~~
Add concerns about effects of moons on Wolfman

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch master
# Changes to be committed:
#	modified:   mars.txt
#
~~~
{: .output}
~~~
[master 34961b1] Add concerns about effects of Mars' moons on Wolfman
 1 file changed, 1 insertion(+)
~~~
{: .output}

Git insists that we add files to the set we want to commit before actually
committing anything. This allows us to commit our changes in stages and capture
changes in logical portions rather than only large batches.  For example,
suppose we're adding a few citations to our supervisor's work to our thesis.
We might want to commit those additions, and the corresponding addition to the
bibliography, but *not* commit the work we're doing on the conclusion (which we
haven't finished yet).

To allow for this, Git has a special *staging area* where it keeps track of
things that have been added to the current [change set]({{ page.root
}}/reference/#change-set) but not yet committed.

> ## Staging Area
>
> If you think of Git as taking snapshots of changes over the life of a
> project, `git add` specifies *what* will go in a snapshot (putting things in
> the staging area), and `git commit` then *actually takes* the snapshot, and
> makes a permanent record of it (as a commit).
>
> If you don't have anything staged when you type `git commit`, Git will prompt
> you to use `git commit -a` or `git commit --all`, which is kind of like
> gathering *everyone* for the picture!  This is, however, bad practice.  It's
> [almost always](http://i.imgur.com/3POtveC.jpg) better to explicitly add
> things to the staging area, because you might commit changes you forgot you
> made.
{: .callout}

![The Git Staging Area](../fig/git-staging-area.svg)

Let's watch as our changes to a file move from our editor to the staging area
and into long-term storage.  First, we'll add another line to the file:

~~~
$ nano mars.txt
$ cat mars.txt
~~~
{: .bash}

~~~
Cold and dry, but everything is my favorite color
The two moons may be a problem for Wolfman
But the Mummy will appreciate the lack of humidity
~~~
{: .output}

~~~
$ git diff
~~~
{: .bash}

~~~
diff --git a/mars.txt b/mars.txt
index 315bf3a..b36abfd 100644
--- a/mars.txt
+++ b/mars.txt
@@ -1,2 +1,3 @@
 Cold and dry, but everything is my favorite color
 The two moons may be a problem for Wolfman
+But the Mummy will appreciate the lack of humidity
~~~
{: .output}

## See changes that are already staged

So far, so good: we've added one line to the end of the file (shown with a `+`
in the first column).  Now let's put that change in the staging area and see
what `git diff` reports:

~~~
$ git add mars.txt
$ git diff
~~~
{: .bash}

There is no output: as far as Git can tell, there's no difference between what
it's been asked to save permanently and what's currently in the directory.
However, if we do this:

~~~
$ git diff --staged
~~~
{: .bash}

~~~
diff --git a/mars.txt b/mars.txt
index 315bf3a..b36abfd 100644
--- a/mars.txt
+++ b/mars.txt
@@ -1,2 +1,3 @@
 Cold and dry, but everything is my favorite color
 The two moons may be a problem for Wolfman
+But the Mummy will appreciate the lack of humidity
~~~
{: .output}

it shows us the difference between the last committed change and what's in the
staging area.

Let's save our changes:

~~~
$ git commit
~~~
{: .bash}
~~~
Discuss concerns about Mars' climate for Mummy

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch master
# Changes to be committed:
#	modified:   mars.txt
#
~~~
{: .output}
~~~
[master 005937f] Discuss concerns about Mars' climate for Mummy
 1 file changed, 1 insertion(+)
~~~
{: .output}

## More details on status and log

Now, let's check our status:

~~~
$ git status
~~~
{: .bash}

~~~
On branch master
nothing to commit, working directory clean
~~~
{: .output}

and look at the history of what we've done so far:

~~~
$ git log
~~~
{: .bash}

~~~
commit 005937fbe2a98fb83f0ade869025dc2636b4dad5
Author: Vlad Dracula <vlad@tran.sylvan.ia>
Date:   Thu Aug 22 10:14:07 2013 -0400

    Discuss concerns about Mars' climate for Mummy

commit 34961b159c27df3b475cfe4415d94a6d1fcd064d
Author: Vlad Dracula <vlad@tran.sylvan.ia>
Date:   Thu Aug 22 10:07:21 2013 -0400

    Add concerns about effects of Mars' moons on Wolfman

commit f22b25e3233b4645dabd0d81e651fe074bd8e73b
Author: Vlad Dracula <vlad@tran.sylvan.ia>
Date:   Thu Aug 22 09:51:46 2013 -0400

    Start notes on Mars as a base

    - Note on temperature
    - Note on water supply
    - Note on energy distribution in the visible spectrum
~~~
{: .output}

This shows a very detailed list of the commits.  Note that it indicates who
committed which changes when and displays the whole commit messages.

> ## Paging the Log
>
> When the output of `git log` is too long to fit in your screen, `git` uses a
> program to split it into pages of the size of your screen.  When this "pager"
> is called, you will notice that the last line in your screen is a `:`,
> instead of your usual prompt.
>
> *   To get out of the pager, press `q`.
> *   To move to the next page, press the space bar.
> *   To search for `some_word` in all pages, type `/some_word` and navigate
>     throught matches pressing `n`.
{: .callout}

> ## Limit Log Size
>
> To avoid that `git log` cover all your terminal screen you can limit the
> numbers of commit that Git will list by using `-N` where `N` is the number of
> commits that you want to receive the information. For example, if you only
> want the information from the last commit you can use
>
> ~~~
> $ git log -1
> ~~~
> {: .bash}
>
> ~~~
> commit 005937fbe2a98fb83f0ade869025dc2636b4dad5
> Author: Vlad Dracula <vlad@tran.sylvan.ia>
> Date:   Thu Aug 22 10:14:07 2013 -0400
>
>    Discuss concerns about Mars' climate for Mummy
> ~~~
> {: .output}
{: .callout}

> ## Compressed Form of the Log
>
> You can also reduce the quantity of information using the
> `--oneline` option:
>
> ~~~
> $ git log --oneline
> ~~~
> {: .bash}
> ~~~
> * 3e246cb Discuss concerns about Mars' climate for Mummy
> * 34961b1 Add concerns about effects of moons on Wolfman
> * f22b25e Start notes on Mars as a base
> ~~~
> {: .output}
>
> You can also combine the `--oneline` options with others. One useful
> combination is
{: .callout}

To recap, when we want to add changes to our repository, we first need to add
the changed files to the staging area (`git add`) and then commit the staged
changes to the repository (`git commit`):

![The Git Commit Workflow](../fig/git-committing.svg)

> ## Choosing a Commit Message
>
> Which of the following commit messages would be most appropriate for the
> last commit made to `mars.txt`?
>
> 1. "Changes"
> 2. "Added line 'But the Mummy will appreciate the lack of humidity' to mars.txt"
> 3. "Discuss effects of Mars' climate on the Mummy"
>
> > ## Solution
> > Answer 1 is not descriptive enough,
> > and answer 2 is too descriptive and redundant,
> > but answer 3 is good: short but descriptive.
> >
> > (But note that answer 3 is best accompagnied by a more detailed description
> > of the commit.)
> {: .solution}
{: .challenge}

> ## Committing Changes to Git
>
> Which command(s) below would save the changes of `myfile.txt`
> to my local Git repository?
>
> 1. `$ git commit`
>
> 2. `$ git init myfile.txt`
>    `$ git commit`
>
> 3. `$ git add myfile.txt`
>    `$ git commit`
>
> 4. `$ git commit -m myfile.txt "my recent changes"`
>
> > ## Solution
> >
> > 1. Would only create a commit if files have already been staged.
> > 2. Would try to create a new repository.
> > 3. Is correct: first add the file to the staging area, then commit.
> > 4. Would try to commit a file "my recent changes" with the message
> >    myfile.txt.
> {: .solution}
{: .challenge}

> ## Committing Multiple Files
>
> The staging area can hold changes from any number of files that you want to
> commit as a single snapshot.
>
> 1. Add some text to `mars.txt` noting your decision to consider Venus as a
>    base
> 2. Create a new file `venus.txt` with your initial thoughts about Venus as a
>    base for you and your friends
> 3. Add changes from both files to the staging area, and commit those changes.
>
> > ## Solution
> >
> > First we make our changes to the `mars.txt` and `venus.txt` files:
> >
> > ~~~
> > $ nano mars.txt
> > $ cat mars.txt
> > ~~~
> > {: .bash}
> > ~~~
> > Maybe I should start with a base on Venus.
> > ~~~
> > {: .output}
> > ~~~
> > $ nano venus.txt
> > $ cat venus.txt
> > ~~~
> > {: .bash}
> > ~~~
> > Venus is a nice planet and I definitely should consider it as a base.
> > ~~~
> > {: .output}
> >
> > Now you can add both files to the staging area. We can do that in one line:
> >
> > ~~~
> > $ git add mars.txt venus.txt
> > ~~~
> > {: .bash}
> >
> > Or with multiple commands:
> >
> > ~~~
> > $ git add mars.txt
> > $ git add venus.txt
> > ~~~
> > {: .bash}
> >
> > Now the files are ready to commit. You can check that using `git status`.
> > If you are ready to commit use:
> >
> > ~~~
> > $ git commit
> > ~~~
> > {: .bash}
> > ~~~
> > Write down my plans to start a base on Venus
> >
> > # Please enter the commit message for your changes. Lines starting
> > # with '#' will be ignored, and an empty message aborts the commit.
> > # On branch master
> > # Changes to be committed:
> > #	modified:   mars.txt
> > #	new file:   venus.txt
> > #
> > ~~~
> > {: .output}
> > ~~~
> > [master cc127c2]
> > Write down my plans to start a base on Venus
> > 2 files changed, 2 insertions(+)
> > ~~~
> > {: .output}
> >
> {: .solution}
{: .challenge}

[commit-messages]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
