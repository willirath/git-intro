---
title: There's more!
teaching: 10
exercises: 0
questions:
- "Is there anything we did not cover?"
objectives:
- "There is!"
keypoints:
---

## Some keywords

- Git-SVN: Allows for migrating / integrating SVN to / in a Git repository.
  To learn more, have a look at the Git Pro book.

- Continuous Integration (or CI): Pushing your commits to a server can trigger
  tasks and jobs (like building a PDF from your thesis or updating your
  website).  To learn more, look for Gitlab's continuous integration of at,
  e.g., <https://travis-ci.org/>.

- Large-File Support: Git is predominanly meant for small to medium sized text
  files (many many thousand lines of code are perfectly fine) but will run into
  problems with large binary files (fully rendered PDFs, data, etc.).  Git-LFS
  will allow for large files to be tracked in a way that ensures a slim
  repository while being able to checkout the large files at any time.  We're
  currently (as of May 2017) in the process of testing <git.geomar.de> with
  very large data repositories.  Talk to the Data Management at Geomar if
  you're interested.

- Cherry picking: You might want to merge only some of the commits from another
  branch.  Check the great Git Pro book for details.

- Rebasing: Similar to merging but rewriting commits in order to maintain a
  linear history.  Again, check the Git Pro book.

--------
