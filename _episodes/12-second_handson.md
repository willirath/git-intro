---
title: Hands-On Session Covering the Second Part
teaching: 10
exercises: 30
questions:
- "How do I create a GitLab project?"
- "How do I clone a remote repository?"
- "How do push local changes?"
- "How do I create Issues, branches, merge requests?"
objectives:
- "As a group, work on a shared repository using GitLab."
keypoints:
- "Issue-tracking, forking and merging across projects allows for a dynamic collaborative environment."
---

Let's use the rest of the course to get familiar with Gitlab and especially
<https://git.geomar.de>.


## Key concepts to explore are:

- Branching on the command line / on the server
- Pulling / pushing branches that were created on the server / locally
- Membership in a project (adding collaborators)
- Proposing changes
  - Issue tracking
  - Merge requests


> ## Work in pairs on a common project
> 
> - Set up a project and add your partner as a collaborator.
> - Add a `README.md`.
> - Both, clone the repository.
> 
{: .challenge}


> ## Edit a file locally and push it to a new branch
> 
> - Essentially re-iterate the Neptune conflict from earlier.
> - Handle it using a merge request online.
> - Look at the history online.  Can you see the commit with two parents?
> 
{: .challenge}


> ## Remove your partner from your project
> 
> Your partner still can propose changes:
> - Fork the repository.
> - Track changes in the fork.  (Ideally, use a dedicated branch for the
>   proposed changes.)
> - Create a merge request from the fork to the original project.
> 
{: .challenge}


> ## Argue in an Issue and resolve in a merge request
> 
> - Let's first make sure both of you are members again...
> - Add an issue detailing some problem.
> - Discuss.
> - Add a branch from within the issue.
> - Fetch and pull this branch and edit locally.
> - Push your changes.  How does this display in the issue?
> - When everything seems fine, resolve in a merge request.
> - How does accepting the merge request affect the issue?
> 
{: .challenge}

--------

## Anything else?

Let's take some time in the end to improvise.

--------
