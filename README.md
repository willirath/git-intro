# git-intro

Course material for an introductory course to Git.

## See the course materials

Check <https://willirath.gitlab.io/git-intro>.

## Building

To build the course material from this source, run `make` and follow the help
given there.

To set up the python environment necessary to convert the course material to a
website, you can use the environment file <git-intro_environment.yml> included
here by running `conda env create -f git-intro_environment.yml` and by
activating the environment with `source activate git-intro`.

You also need `jekyll`.  On Ubuntu, install it with:
```bash
sudo apt-get install jekyll
```

## Credit

The material is based on <https://swcarpentry.github.io/git-novice/> from the
[Software Carpentry Foundation][software-carpentry].

[software-carpentry]: https://software-carpentry.org
